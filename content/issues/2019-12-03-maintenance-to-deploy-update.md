---
section: issue
title: Maintenance to deploy update
date: 2019-12-03T21:22:47.339Z
resolved: true
resolvedWhen: 2019-12-03T22:04:49.637Z
affected:
  - MGG Server
severity: notice
---
*Maintenance* - An update is being deployed to fix an issue related to missing achievements.
