---
section: issue
title: Planned Maintenance
date: 2019-11-15T23:00:52.906Z
resolved: true
resolvedWhen: 2019-11-16T00:00:52.952Z
affected:
  - MGG Server
severity: notice
---
_Maintenance_ - The game server is currently down to deploy an update.
