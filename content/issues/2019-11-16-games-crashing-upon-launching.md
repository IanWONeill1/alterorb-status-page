---
section: issue
title: Games Crashing Upon Launching
date: 2019-11-16T14:09:54.021Z
resolved: true
resolvedWhen: 2019-11-16T15:00:56.699Z
affected:
  - MGG Server
severity: disrupted
---
_Investigating_ - An issue related to highscores is causing the games to crash upon login, this is now being investigated.

_Resolved_ - The "My Scores" and "Best Each" highscores tabs have been temporarily disabled.
